var Discordbot = require('discord.io');
var request = require('request');
var fs = require('fs');

var bot = new Discordbot({
    email: "tt1@cs.au.dk",
    password: "chatbot",
    autorun: true
});

var prefix = "[x] \t";

bot.on("err", function(rawEvent) {
    console.error(prefix + rawEvent);
});

bot.on("ready", function(rawEvent) {
    console.log(prefix + "ready.");
});

bot.on("message", function(user, userid, channelid, message, rawEvent) {
    console.log(prefix + channelid + "\\" + user + ":\t"+message);
    handlemessage(channelid, message);
});

bot.on("presence", function(user, userid, status, rawEvent) {
    console.log(prefix + user + " is now " + status);
});

bot.on("debug", function(rawEvent) {
    //console.warn(prefix + "debug:" + rawEvent);
});

bot.on("disconnect", function() {
    console.log(prefix + "disconnected, reconnecting.");
    bot.connect();
});


function handlemessage(channelid, msg){
    var tokens = msg.split(" ");

    var idx = -1;
    if ((idx = msg.indexOf("$$")) >= 0){
        doLatex(channelid, msg);
        return;
    }

    if (msg.indexOf("linux") >= 0){
        bot.sendMessage({
            to: channelid,
            message: "WOW MUCH AMAZE"
        });
        
        return;
    }
}

function doLatex(channelid, msg){
    var reply = between("$$", msg, "$$");
    if (reply != ""){
        
        var url = "https://latex.codecogs.com/png.latex?"+
            encodeURIComponent(reply);
        console.log(prefix + "url:" + url);
        
        request.get(url)
            .on("error",function(err){
                console.error(prefix + err);
                return;
            })
            .pipe(fs.createWriteStream('latex.png'))
            .on("finish", function() {
                bot.uploadFile({
	            channel: channelid,
	            file: 'latex.png'
	        });
            });
    }
}

function between(left, message, right){
    var leftidx = message.indexOf(left);
    var rightidx = message.substring(leftidx+left.length).indexOf(right);

    return message.substring(leftidx+left.length, leftidx+left.length+rightidx+1);
}
