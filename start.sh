#!/bin/bash

# install nodejs and npm
yes | sudo pacman -S --needed npm nodejs icu
yes | npm install discord.io request

while true
do
    node app.js
done
